# Tor Router 

Tor Router allow you to use TOR as a transparent proxy and send all your trafic under TOR **INCLUDING DNS REQUESTS**
This version of Tor Router can be runed in systemd only :)


# Instalation :

## Manual :
**It script require root privileges**
1. Open a terminal and clone the script using the following command:
```
→ git clone https://gitlab.com/Abdennour.py/tor-router.git
→ cd tor-router
→ chmod +x *
→ sudo ./install.sh
```
2. Put the following lines at the end of /etc/tor/torrc
```
# Seting up TOR transparent proxy for tor-router
VirtualAddrNetwork 10.192.0.0/10
AutomapHostsOnResolve 1
TransPort 9040
DNSPort 5353
```
3. Start the tor service :
```
sudo systemctl start tor.service
```
4. Execute the tor-router script as root
```
sudo ./tor-router
```
## BlackArch :
```
pacman -S tor-router
```
## Archlinux :
```
→ sudo pacman -Sy tor
→ yay -S tor-router
→ sudo systemctl start tor.service
→ sudo tor-router
```

